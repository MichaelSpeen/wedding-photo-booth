var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var LED = new Gpio(4, 'out'); //use GPIO pin 4 as output
var pushButton = new Gpio(17, 'in', 'both'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled

var nrc = require('node-run-cmd');
var SAVED_INFO_OUTPUT = "Saving file as ";
var DONE_INFO_OUTPUT = "Deleting file";
var fs = require('fs');

var dataCallback = function(code) {
  if(code.indexOf(SAVED_INFO_OUTPUT) !== -1){
    nrc.run("gpicview " + code.split(" ")[11]);
  }
  if(code.indexOf(DONE_INFO_OUTPUT) !== -1){
    LED.writeSync(1)
  }
  console.log(code);
  console.log("-----------");
};

LED.writeSync(1); //turn LED on or off (0 or 1)

pushButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
  if (err) { //if an error
    console.error('There was an error', err); //output error message to console
    return;
  }
  if(value){
    nrc.run("gphoto2 --capture-image-and-download", { onData: dataCallback});
    LED.writeSync(0);
  }
  
});

function unexportOnClose() { //function to run when exiting program
  LED.writeSync(0); // Turn LED off
  LED.unexport(); // Unexport LED GPIO to free resources
  pushButton.unexport(); // Unexport Button GPIO to free resources
};

process.on('SIGINT', unexportOnClose); //function to run when user closes using ctrl+c
