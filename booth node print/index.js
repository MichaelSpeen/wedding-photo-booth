var fs = require('fs');
var request = require("request");
var exec = require('child_process').exec;

var host = "localhost";
var url = "http://" + host + ":8082/printlist"

var printQueue = [];


var loadData = 
	function(){
		if(printQueue.length > 0){
			console.log("printQueue has " + printQueue.length + " lements -> skipping api call");
			return;
		}
		
		request({
			url: url,
			json: true
		}, function (error, response, body) {

			if (!error && response.statusCode === 200) {
				printQueue = body;
				console.log("new print queue received:")
				console.log(body) // Print the json response
				
				processPrint();
			}
		});
	}

loadData();
//setInterval(()=>{loadData()}, 10000);


var download = function(uri, filename, callback){
	console.log("downloading: " + uri);
  request.head(uri, function(err, res, body){
    if (err) callback(err, filename);
    else {
        var stream = request(uri);
        stream.pipe(
            fs.createWriteStream(filename)
                .on('error', function(err){
                    callback(error, filename);
                    stream.read();
                })
            )
        .on('close', function() {
            callback(null, filename);
        });
    }
  });
};

var processPrint = function(){
	console.log("starting printing... ( " + printQueue.length + " photos left)")
	
	//load filename from queue
	let filename = printQueue.pop() + ".jpg";
	
	//download file
	download('http://'+host+':8082/file/'+filename, "print/"+filename, function(){
	  console.log('done');
	  continueProcessPrint("print/"+filename);
	});
}

var continueProcessPrint = function(filename){
	if(getFileMB(filename) > 0.05){
		//print only if download was successful
		doPrinting(filename);	
	} else console.log(getFileMB(filename) + "skipping printing of " + filename);
	
	//then print other photo (if available) 
	if(printQueue.length > 0)
		setTimeout(()=>{
			processPrint();
		},4000);
}

var getFileMB = function(filename){
	const stats = fs.statSync(filename)
	const fileSizeInBytes = stats.size
	const fileSizeInMegabytes = fileSizeInBytes / 1000000.0
	return fileSizeInMegabytes
}


var doPrinting = function(filename){
	console.log("==== printing: " + filename)
	exec('mspaint /pt ' + filename, function(err, data) {console.log(data.toString());});
}




