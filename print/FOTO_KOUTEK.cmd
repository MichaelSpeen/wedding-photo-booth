@echo off

rem TODO send clear request

Set /A COUNTER=1
Set /A CLEAR=1

:loop

	call :check_printer_connected COUNTER
	if %errorlevel% == 0 (
		rem TODO run script with clear parameter
		rem call _loop_me.cmd CLEAR
	) else (
		rem set clear indicator to clear 
		Set /A CLEAR=1
	)

	rem sleep 1sec
	ping 127.0.0.1 -n 2 > nul
	set /A COUNTER=COUNTER+1

goto loop





rem https://www.tutorialspoint.com/batch_script/batch_script_functions.htm


:check_printer_connected

	set /A PARAM = %~1
	set /a even=PARAM %% 2
	if %even%==0 (
		set prefix=-
	) else (
		set prefix=+
	)
	
	rem https://stackoverflow.com/questions/12453733/search-for-a-string-in-command-line-output
	wmic printer get name,attributes | findstr /I "Alison" | findstr /I "68" > nul
	if %errorlevel% == 0 (
	   echo %prefix% PeriPage je OK
	   EXIT /B 0
	) ELSE (
	   echo %prefix% PeriPage je ODPOJENO
	   EXIT /B 1
	)
	
	

	
