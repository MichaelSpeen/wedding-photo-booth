@echo off
set CLEAR_PARAMETER=%~1

set HOST=http://localhost:8082
set QUEUE_URL=%HOST%/queued
set FILE_URL=%HOST%/file/


call :get_photo_file_name_from_queue %QUEUE_URL% photo_file_name
if %errorlevel% == 1 (
	EXIT /B 0
)

set PHOTO_URL=%FILE_URL%%photo_file_name%
call :download_photo %PHOTO_URL% %photo_file_name%

call :print_photo %photo_file_name%

EXIT /B 0



:get_photo_file_name_from_queue
	_wget.exe -qO temp.txt %~1
	set /p result=<temp.txt
	set EMPTY=empty
	if %result% == %EMPTY% (
		:: there is nothing to print
		EXIT /B 1
	)
	echo Fotka v tiskove fronte: %result%
	set %~2=%result%.jpg
EXIT /B 0


:download_photo
	:: download photo only if it was not downloaded yet
	if exist %~2 (
		echo Fotka uz byla stazena drive: %photo_file_name%
	) else (
		echo Bude se stahovat nova fotka: %photo_file_name%
		_wget.exe %~1
	)
EXIT /B 0


:print_photo
	:: https://irfanview-forum.de/showthread.php?t=6559
	:: Example:
	:: C:\IrfanView\i_view32.exe "C:\My Pictures\image 01.jpg" /ini="C:\IrfanView\myConfigs\Print\" /print="Name of your preferred printer"
	
	rem echo print disabled --- %~1
	data\i_view64.exe %~1 /ini="data\i_view64.ini" /print="Alison A6"
EXIT /B 0


:get_photo_file_name
	set PATHURL=%~1
	For %%A in ("%PATHURL%") do (
		Set Folder=%%~dpA
		Set Name=%%~nxA
	)
	echo.Folder is: %Folder%
	echo.Name is: %Name%
	
	set %~2=Name
EXIT /B 0