/**
 * Created by michael.bezvoda.ext on 3.3.2019.
 */
var express = require('express');
var lists = require('./lists.js');
var app = express();


app.get('/', function (req, res) {
    res.send('Hello World');
})
app.get('/queued', function (req, res) {
    res.send(lists.printQueue.length ? lists.printQueue.pop() : "empty");
})
app.use('/file', express.static('photos'))

var server = app.listen(8082, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Fotokoutek is running at http://%s:%s", host, port)
})