var request = require('request');
var fs = require('fs');

var lists = require('../lists.js');

setInterval(()=>{
  fs.readdir(".", function(err, items) {
    for (var i=0; i<items.length; i++) {
      if(items[i].endsWith(".jpg") && items[i] !== ("bg.jpg")){
        var queueContainsFileAlready = (lists.printPreprocessingQueue.indexOf(items[i]) > -1);
        if(!queueContainsFileAlready)
          lists.printPreprocessingQueue.push(items[i]);
        break;
      }
    }
  });
}, 2000);

