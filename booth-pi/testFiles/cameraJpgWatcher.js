/**
 * Created by michael.bezvoda.ext on 3.3.2019.
 */
var request = require('request');
var fs = require('fs');

var imgQueue = [];
exports.imgQueue = imgQueue;

var self = this;


setInterval(()=>{
    console.log("setInterval started",imgQueue);

    fs.readdir(".", function(err, items) {
        for (var i=0; i<items.length; i++) {
            if(items[i].endsWith(".jpg")){
                console.log("IMG found",items[i]);
                self.processNewJpg(items[i]);
                break;
            }
        }
    });

},1000);


this.processNewJpg = function (fileName) {
    //find new fileName
    let uniqueName = getUniqueFilename();

    //copy to photos folder
    fs.rename(fileName, 'photos/' + uniqueName + ".jpg", function() {
        //add to queue for print preparation
        imgQueue.push(uniqueName);

        //upload to web
        uploadToWeb(uniqueName);
    });
};

function uploadToWeb(filename) {

    var data = {
        file: fs.createReadStream('photos/' + filename + ".jpg")
    };
    request.post({
        url: 'http://veronikakyralova.michaelbezvoda.cz/booth/upload.php',
        formData: data
    }, function callback(err, response, body) {
        if (err) {
            return console.error('Failed to upload:', err);
        }
        console.log('Upload successful!');
    });
}
function getUniqueFilename() {
    let r = Math.random().toString(36).substring(8).toUpperCase();
    if (fs.existsSync("photos/" + r + ".jpg")) {
        return getUniqueFilename();
    }
    console.log("getUniqueFilename", r);
    return r;
}