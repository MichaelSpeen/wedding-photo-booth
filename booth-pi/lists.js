const LOG_NAME = "QUEUE";


var printQueue =
    new Proxy([], { set(target, property, value) { if(!isNaN(property))
        console.log(LOG_NAME,"printQueue pushed",value); return Reflect.set(target, property, value); }});

var printPreprocessingQueue =
    new Proxy([], { set(target, property, value) { if(!isNaN(property))
        console.log(LOG_NAME,"printPreprocessingQueue pushed",value); return Reflect.set(target, property, value); }});

exports.printQueue = printQueue;
exports.printPreprocessingQueue = printPreprocessingQueue;