/**
 * Created by michael.bezvoda.ext on 3.3.2019.
 */
var lists = require('./lists.js');
var request = require('request');
var jimp = require('jimp');
var qr = require('qr-image');
var fs = require('fs');

const LOG_NAME = "PREPROCESSING";
const UPLOAD_URL = 'http://veronikakyralova.michaelbezvoda.cz/booth/upload.php';
const BROWSE_URL = 'http://photo.speen.cz?p=';

const PROMISE_TYPE_UPLOADER = "UPLOADER";
const PROMISE_TYPE_IMG_PROCESSOR = "IMG_PROCESSOR";

var running = false;
setInterval(()=>{
    if(running || lists.printPreprocessingQueue.length <= 0) {
        //console.log(LOG_NAME, "already running or queue empty");
        return;
    }

    try {
        running = true;
        startPreprocessing(lists.printPreprocessingQueue.pop())
    } catch(e) {
        console.log("ERROR",e)
    } finally {
        running = false;
    }
},500);


function findUniqueFileName() {
    let randomName = Math.random().toString(36).substring(3).toUpperCase();
    if (fs.existsSync("photos/" + randomName + ".jpg")) {
        return findUniqueFileName();
    }
    return randomName;
}

function renameAndCopyFileToFolder(fileName, uniqueFilename) {
    return new Promise((resolve, reject) => {
        let uniqueFilePath = 'photos/' + uniqueFilename + ".jpg";
        fs.rename(fileName, uniqueFilePath, function() {
            resolve(uniqueFilePath);
        });
    });
}

function getNewUploader(uniqueFilePath) {
    return new Promise((resolve, reject) => {
        var data = {
            file: fs.createReadStream(uniqueFilePath)
        };
        request.post({
            url: UPLOAD_URL,
            formData: data
        }, function callback(err, response, body) {
            if (err) {
                console.error('Failed to upload:', err);
                //ignore reject(err);
                resolve(createResolveObject(PROMISE_TYPE_UPLOADER,JSON.stringify(err)));
            }
            console.log('Upload successful!');
            resolve(createResolveObject(PROMISE_TYPE_UPLOADER,body));
        });
    });
}

function createResolveObject(type, content) {
    return { type: type, resultContent: content }
}

function createQR(uniqueFilename) {
    qr.image(BROWSE_URL + uniqueFilename, { type: 'png', margin: 0 }).pipe(fs.createWriteStream('qr.png'));
}
function mergeImages(uniqueFilename,uniqueFilePath) {
    //https://github.com/jessguilford/how-to-composite-with-jimp/blob/master/composite.js
    //https://dustinpfister.github.io/2017/04/10/nodejs-jimp/
    return new Promise((resolve, reject) => {
        jimp.read('bg.jpg').then(function (imgBg) {
            jimp.read(uniqueFilePath).then(function (imgPhoto) {
                imgPhoto.scaleToFit(685, jimp.AUTO, jimp.RESIZE_BEZIER)
                imgBg.composite(imgPhoto,0,0);
                let name = uniqueFilename+'_p.jpg';
                imgBg.write("photos/"+name, function() {
                    console.log("wrote the image");
                    resolve(name);
                });
            });
        }).catch (function (err) {
            console.log(err)
        });
    });

}
function getNewImageProcessor(uniqueFilename,uniqueFilePath) {
    return new Promise((resolve, reject) => {
        createQR(uniqueFilename);
        mergeImages(uniqueFilename,uniqueFilePath).then(f => {
            resolve(createResolveObject(PROMISE_TYPE_IMG_PROCESSOR,f));
        });
    });
}

function addPreprocessedImageToPrintQueue(values) {
    for(let i = 0 ; i<values.length; i++){
        let promiseResult = values[i];
        if(promiseResult.type === PROMISE_TYPE_IMG_PROCESSOR){
            lists.printQueue.push(promiseResult.resultContent);
        }
    }
}

function startPreprocessing(fileName) {
    console.log(LOG_NAME,"started",fileName);

    let uniqueFilename = findUniqueFileName();
    console.log(LOG_NAME,"new unique filename generated", uniqueFilename);

    renameAndCopyFileToFolder(fileName,uniqueFilename).then((uniqueFilePath) => {
        let uploader = getNewUploader(uniqueFilePath);
        let imageProcessor = getNewImageProcessor(uniqueFilename,uniqueFilePath);

        Promise.all([uploader,imageProcessor]).then((values)=>{
            console.log(LOG_NAME,"done",values);
            addPreprocessedImageToPrintQueue(values);
        });
    });
}